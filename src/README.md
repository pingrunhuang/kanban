# Component Lifecycle
*Mounting*: `class constructor` -> `componentWillMount` -> `render` -> `componentDidMount`  
*Unmounting*: `componentWillUnmount` -> `componentWillReceiveProps` -> `shouldComponentUpdate` -> `componentWillUpdate` ->
`render` -> `componentDidUpdate`  
*State changes*: `shouldComponentUpdate` -> `componentWillUpdate` -> `render` -> `componentDidUpdate`


# Problems met and how to solve with React
## 1. Nested Objects
  In general programming, if we want to add a new element to an existing array, we could simply call the array method like `push()`. But in javascript, object are immutable after creation. 2 optional method could be used here: `array.concat()` or `Object.assign({}, a, b)`. Under the hood, 2 objects will be created. Say I want to change the state of b, the value of the final object will be affected since javascript is using reference assignment to assign value.

  *Solution provided by React*: Immutability helper
  How does it work?
