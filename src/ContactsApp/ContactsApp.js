import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import 'whatwg-fetch';

class ContactsAppContainer extends Component {
	constructor() {
		super();
		this.state={
			contacts:[]
		}
	}
	componentDidMount() {
		fetch('./contacts.json').then((response) => response.json()).then((responseData) => {
			this.setState({contacts: responseData});
		}).catch((error) => {
			console.log('Error fetching and parsing data', error);
		})
	}

	render() {
		return (
			<ContactsApp contacts={this.state.contacts} />
		)
	}
}


// This app demonstrate how to make the component stateful
class ContactsApp extends Component {
	constructor() {
		super();
		this.state = {
			filterText: ''
		}
	}
	render() {
		return (
			<div className='contactsApp'>
			<SearchBar filterText={this.state.filterText}/>
			<ContactList contacts={this.props.contacts} filterText={this.state.filterText}/>
		</div>)
	}
}

ContactsApp.propType = {
	contacts: PropTypes.arrayOf(PropTypes.Object)
}

class SearchBar extends Component {
	render(){
		return (<input type='search' placeholder='search'
							value={this.props.filterText} />)
	}
}

SearchBar.propTypes = {
	filterText: PropTypes.string.isRequired
}

class ContactList extends Component {

	render() {
		let filteredContacts = this.props.contacts.filter(
			(contact) => contact.name.indexOf(this.props.filterText) !== -1
		)
		return(
			<ul>
				{
					filteredContacts.map((contact) => <ContactItem
								key={contact.email}
								name={contact.name}
								email={contact.email} />)
				}
			</ul>
		)
	}
}

ContactList.propTypes = {
	contacts: PropTypes.arrayOf(PropTypes.Object)
}

class ContactItem extends Component {
	render() {
		return(
			<li>{this.props.name} - {this.props.email}</li>
		)
	}
}

// let contacts = [
//   { name: "Cassio Zen", email: "cassiozen@gmail.com" },
//   { name: "Dan Abramov", email: "gaearon@somewhere.com" },
//   { name: "Pete Hunt", email: "floydophone@somewhere.com" },
//   { name: "Paul O’Shannessy", email: "zpao@somewhere.com" },
//   { name: "Ryan Florence", email: "rpflorence@somewhere.com" },
//   { name: "Sebastian Markbage", email: "sebmarkbage@here.com" },
// ]
// ReactDOM.render(<ContactsAppContainer />, document.getElementById('root'))
