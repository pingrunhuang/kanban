// This component is a container for data fetching and persistence

import React, { Component } from 'react';
import KanbanBoard from './KanbanBoard';
import 'whatwg-fetch';
// this polyfill
import 'babel-polyfill';
import { cardsList } from './data.js';
import update from 'react-addons-update';

const IS_DEBUG = true;
const API_URL = 'http://kanbanapi.pro-react.com';
const API_HEADERS = {
  'Content-Type': 'application/json',
  Authorization: 'hello world'
}
class KanbanBoardContainer extends Component {
  constructor() {
    super()
    this.state = {
      cards:[],
    };
  }

  // this method is going to update the status of each card
  updateCardStatus(cardId, updatedCardStatus) {
    let cardIndex = this.state.cards.findIndex( (card) => card.id === cardId);
    let card = this.state.cards[cardIndex];

    // the goal is to put the target card to another list
    if(card.status !== updatedCardStatus){
      this.setState(
        update(
            this.state, {
              cards: {
                [cardIndex]: {
                  status: {$set: updatedCardStatus}
                }
              }
            }
        )
      )
    };
  }

  updateCardPosition(cardId, afterId){
    if (cardId !== afterId) {
      let cardIndex = this.state.cards.findIndex((card) => card.id === cardId);
      let card = this.state.cards[cardIndex];
      let afterIndex = this.state.cards.findIndex((card)=> card.id === afterId);
      this.setState(
        update(
          this.state, {
            cards: {
              $splice: [
                [cardIndex,1],
                [afterIndex,0,card]
              ]
            }
          }
        )
      )
    }
  }

  componentDidMount(){
    if(IS_DEBUG){
      this.setState({cards: cardsList})
    }else{
      fetch(API_URL + '/cards', {headers:API_HEADERS}).then((response) => response.json()).then((data) => {
        this.setState({cards: data});
      }).catch( (error) => {console.log("Error fetching and parsing data", error);})
    };
  }

  addTask(cardId, taskName){
    let prevState = this.state;
    let cardIndex = this.state.cards.findIndex((card) => card.id === cardId);
    // use a temperary id
    let newTask = {id: Date.now(), name: taskName, done: false};

    // it is getting the object based on the index, that is why we got the square brackets here
    let nextState = update(this.state.cards, {
      [cardIndex]: {
        tasks: {$push: [newTask]}
      }
    });
    this.setState({cards: nextState});

    // call api
    fetch(`${API_URL}/cards/${cardId}/tasks`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(newTask)
    }).then(
      (response) => {
        if(!response.ok){
          throw new Error("Server response is not ok!")
        }
        response.json()
      }
    ).then(
      (data) => {
        // persist the id
        newTask.id = data.id
        this.setState({cards: nextState});
      }
    ).catch(
      (error) => {
        console.log('fetch error:', error)
        this.setState(prevState)
      }
    );
  }

  deleteTask(cardId, taskId, taskIndex){
    let prevState = this.state;
    let cardIndex = this.state.cards.findIndex((card) => card.id === cardId);

    // use react's immutability helpers to not modify the current state directly
    // update a new object without the task
    let nextState = update(this.state.cards, {
      [cardIndex]: {
        tasks: {$splice: [[taskIndex, 1]]}
      }
    });

    // update the local changes
    this.setState({cards:nextState});

    // call the API to remove the task on the server
    fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`, {
      method: 'delete',
      headers: API_HEADERS
    }).then(
      (response) => {
        if(!response.ok){
          throw new Error("Server response is not ok!")
        }
      }
    ).catch(
      (error) => {
        console.log('fetch error:', error)
        this.setState(prevState)
      }
    );;

  }

  toggleTask(cardId, taskId, taskIndex){
    // set to done or not done
    let prevState = this.state;
    let cardIndex = this.state.cards.findIndex((card) => card.id === cardId);
    let newDoneValue;
    let nextState = update(this.state.cards, {
      [cardIndex]: {
        tasks: {
          [taskIndex]: {
            done: {
              $apply: (done) => {
                newDoneValue=!done
                return newDoneValue;}
            }
          }
        }
      }
    });
    this.setState({cards: nextState});
    fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`, {
      method: 'put',
      headers: API_HEADERS,
      body: JSON.stringify({done: newDoneValue})
    }).then(
      (response) => {
        if(!response.ok){
          throw new Error("Server response is not ok!")
        }
      }
    ).catch(
      (error) => {
        console.log('fetch error: ', error)
        this.setState(prevState)
      }
    );
  };

  render() {
    return <KanbanBoard cards={this.state.cards}
    taskCallbacks={
      {
        toggle: this.toggleTask.bind(this),
        delete: this.deleteTask.bind(this),
        add: this.addTask.bind(this)
      }
    }
    cardCallbacks={
      {
        updateCardPosition: this.updateCardPosition.bind(this),
        updateCardStatus: this.updateCardStatus.bind(this)
      }
    }
    />
  }
}

export default KanbanBoardContainer;
