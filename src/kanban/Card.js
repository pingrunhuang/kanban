import React, { Component } from "react";
import PropTypes from 'prop-types';
import CheckList from "./CheckList";
import marked from 'marked';
import {color_map} from './data.js';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { DragSource, DropTarget } from 'react-dnd';
import constants from './constants';


// define customized propType validator
let customizedTitlePropType = (props, propName, componentName) => {
	if(props[propName]) {
		let value = props[propName];
		if (typeof value !== 'string' || value.length > 80) {
			return new Error(
				`${propName} in ${componentName} is longer than 80 characters`
				);
		}
	}
}

// card object is the drag source
const cardDragSpec = {
	beginDrag(props) {
		return {
			id: props.id
		};
	}
}

// chaging the card into both drag source and drop target
const cardDropSpec = {
	hover(props, monitor){
		const draggedId = monitor.getItem().id;
		// the draggedId is the id of current card that is being dragged
		// the props.id is the target card that is going to be dropped
		props.cardCallbacks.updateCardPosition(draggedId, props.id);
	}
}

let dragCollect = (connect, monitor) => {
	return {
		connectDragSource: connect.dragSource()
	};
}

let dropCollect = (connect, monitor) => {
	return {
		connectDropTarget: connect.dropTarget()
	};
}


class Card extends Component {
	/* define the initial state for the card to have the toggle ability */
	constructor(props) {
		super(props);
		this.state={
			showDetails: false,
		};
	}
	toggleDetails() {
		this.setState({showDetails: !this.state.showDetails})
	}

	render() {
		const {connectDragSource, connectDropTarget} = this.props;
		let cardDetails;
		if (this.state.showDetails){
			cardDetails = (
				<div className="card__details">
					<span dangerouslySetInnerHTML={{__html: marked(this.props.description)}} />
					<CheckList cardId={this.props.id} tasks={this.props.tasks} taskCallbacks={this.props.taskCallbacks}/>
				</div>
				);
		}

		// how the inline style works
		let sideColor = {
			position: 'absolute',
			zIndex: -1,
			top: 0,
			bottom: 0,
			left: 0,
			width: 7,
			backgroundColor: color_map[this.props.status]
		};
		return connectDropTarget(
			connectDragSource(
				<div className="card">
					<div style={sideColor} />
					{/* how to apply conditional check in attribute */}
					<div className={this.state.showDetails? "card__title card__title--is-open" : "card__title"} onClick={this.toggleDetails.bind(this)}>{this.props.title}
					</div>
					<ReactCSSTransitionGroup transitionName='cardDetail'
						transitionEnterTimeout={250}
						transitionLeaveTimeout={250}>
						{cardDetails}
					</ReactCSSTransitionGroup>
				</div>
				)
		);
	}
}
Card.propsTypes = {
	id: PropTypes.number,
	tasks: PropTypes.arrayOf(PropTypes.object),
	title: customizedTitlePropType,
	description: PropTypes.string,
	status:PropTypes.string,
	cardDetails: PropTypes.arrayOf(PropTypes.object),
	taskCallbacks: PropTypes.object,
	cardCallbacks: PropTypes.object,
	connectDropTarget: PropTypes.func.isRequired,
	connectDragSource: PropTypes.func.isRequired
}

const dragHighOrderCard = DragSource(constants.CARD, cardDragSpec, dragCollect)(Card);
const dropHighOrderCard = DropTarget(constants.CARD, cardDropSpec, dropCollect)(dragHighOrderCard);

export default dropHighOrderCard;
