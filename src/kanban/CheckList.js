import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CheckList extends Component {

	// to check if the user pressed the Enter key
	checkInputKeyPress(e){
		if(e.key === 'Enter'){
			this.props.taskCallbacks.add(this.props.cardId, e.target.value);
			// clear the input field after invoking add
			e.target.value = '';
		}
	}

	render() {
		let tasks = this.props.tasks.map((task, taskIndex) => (
			<li className="checkList__task" key={task.id}>
				{/* this is controlled component */}
				<input type="checkbox" defaultChecked={task.done} onChange={
					this.props.taskCallbacks.toggle.bind(null, this.props.cardId, task.id, taskIndex)
				}/>
				{task.name}{' '}
				<a href="#" className="checkList__task--remove" onClick={
					this.props.taskCallbacks.delete.bind(null, this.props.cardId, task.id, taskIndex)
				}/>
			</li>
			));
		return (
			<div className="checkList">
				<ul>{tasks}</ul>
				{/* this is uncontrolled component */}
				<input type="text"
						className="checkList--add-task"
						placeholder="Type then hit Enter to add a task"
						onKeyPress={this.checkInputKeyPress.bind(this)}/>
			</div>
		)
	}
}
CheckList.propType = {
	tasks: PropTypes.arrayOf(PropTypes.object),
	taskCallbacks: PropTypes.object,
	cardId: PropTypes.number
}
export default CheckList;
