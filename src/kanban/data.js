export var cardsList = [
	{
		"id": 1,
		"title": "Read the Hyperspace book",
		"description": "I should read the book to get deeper understanding of higher dimension",
		"status": "in-progress",
		"tasks": []
	},
	{
		"id": 2,
		"title": "Create an ruby on rail based API application",
		"description": "I should finish this project before applying to Airbnb",
		"status": "todo",
		"tasks": [
			{
				"id": 1,
				"name": "Grab a ruby book",
				"done": true
			},
			{
				"id": 2,
				"name": "Start coding based on the restful API rules",
				"done": false
			},
			{
				"id": 3,
				"name": "Push to github repo",
				"done": false
			}
		]
	},
	{
		"id": 3,
		"title": "build a decentralized application",
		"description": "Blockchain is really a awesome technology. Some startups were starting to use it in the field such as [arcade city](https://arcade.city). I imagine it might be a good choice for education.",
		"status": "todo",
		"tasks": []
	},
	{
		"id": 4,
		"title": "kanban application based on react",
		"description": "Coding while reading the [pro react](https://github.com/pingrunhuang/bookList/blob/master/javascript/pre_react.pdf) book",
		"status": "done",
		"tasks": [
			{
				"id": 1,
				"name": "create the backbone of the app with react",
				"done": true
			},
			{
				"id": 2,
				"name": "enrich the content",
				"done": false
			}
		]
	}
];
export var color_map = {
	"done": "#BD8D31",
	"in-progress": "#3A7E28",
	"todo": "	#0000FF"
}
