import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from './Card';
import constants from './constants';
import {DropTarget} from 'react-dnd';

const listTargetSpec = {
	hover(props, monitor) {
		const draggedId = monitor.getItem().id;
		props.cardCallbacks.updateCardStatus(draggedId, props.id);
		// props.cardCallbacks.updateCardPosition(draggedId, props.cards.id);

	}
}

let collect = (connect, monitor) => {
	return {
		connectDropTarget: connect.dropTarget()
	};
}

class List extends Component {
	render (){
		const {connectDropTarget} = this.props;
		let cards = this.props.cards.map((card) => {
			// react need the key to match and avoid performance bottlenecks like operation ambiguity
			return <Card key={card.id}
						id={card.id}
						title={card.title}
						description={card.description}
						tasks={card.tasks}
						status={card.status}
						taskCallbacks={this.props.taskCallbacks}
						cardCallbacks={this.props.cardCallbacks}/>
		});

		return connectDropTarget(
			<div className="list">
				<h1>{this.props.title}</h1>
				{cards}
			</div>
		)
	}
};


List.propTypes = {
	title: PropTypes.string.isRequired,
	cards: PropTypes.arrayOf(PropTypes.object),
	taskCallbacks: PropTypes.object,
	cardCallbacks: PropTypes.object,
	connectDropTarget: PropTypes.func.isRequired
};

export default DropTarget(constants.CARD, listTargetSpec, collect)(List);
