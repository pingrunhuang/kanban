import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
// import './index.css';
// import App from './App';



// import ContactsApp from './ContactsApp/ContactsApp';


// import KanbanBoard from './kanban/KanbanBoard';
// ReactDOM.render(<KanbanBoard cards={cardsList} />, document.getElementById('root'));

import KanbanBoardContainer from './kanban/KanbanBoardContainer';
ReactDOM.render(<KanbanBoardContainer />, document.getElementById('root'));

// import AnimatedShoppingList from './animatedShoppingList/AnimatedShoppingList';
// ReactDOM.render(<AnimatedShoppingList />, document.getElementById('root'));

// import MallContainer from './animatedShoppingList/MallContainer';
// ReactDOM.render(<MallContainer />, document.getElementById('root'));

registerServiceWorker();
