import React, { Component } from './react';

class FocusText extends Component {
	handleClick() {
		this.ref.myTextInput.focus();
	}
	render() {
		<div>
			<input type="text" ref="myTextInput">
			<input 
				type="button"
				value="Focus the text input"
				onClick={this.handleClick.bind(this)} />
		</div>
	}	

}