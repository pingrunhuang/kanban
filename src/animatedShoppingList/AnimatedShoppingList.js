import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import './AnimatedShoppingList.css';

class AnimatedShoppingList extends Component {

  constructor(){
    super();
    this.state={
      items: [
        {id: 1, name:'Milk'},
        {id: 2, name:'Yogurt'},
        {id: 3, name:'Tank'}
      ]
    }
  }

  // get called when user change the input field
  handleChange(e){
    if(e.key === 'Enter'){
      // create new item
      let newItem = {id: Date.now(), name: e.target.value};
      let newItems = this.state.items.concat(newItem);
      e.target.value='';
      this.setState({items: newItems});
    }
  }

  handleRemove(index){
    let newItems = this.state.items;
    newItems.splice(index, 1);
    this.setState({items: newItems});
  }

  render(){

    let shoppingItems = this.state.items.map((item, i) => (
      <div key={item.id} className="item"
           onClick={this.handleRemove.bind(this, i)}>
        {item.name}
        </div>
      )
    );
    return(
      <div>
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}
          transitionAppear={true}
          transitionAppearTimeout={300}>
          {shoppingItems}
        </ReactCSSTransitionGroup>
        <input type="text" value={this.state.newItems} onKeyDown={this.handleChange.bind(this)}/>
      </div>
    )
  }
}

export default AnimatedShoppingList;
