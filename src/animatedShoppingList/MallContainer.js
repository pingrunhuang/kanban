// drag and drop example
import React, { Component } from 'react';
import ShoppingCart from './ShoppingCart';
import Snack from './Snack';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import './Mall.css';

class Container extends Component {
  render() {
    return(
      <div>
        <Snack name="chips" />
        <Snack name="cupcake" />
        <Snack name="donut" />
        <Snack name="doritos" />
        <Snack name="popcorn" />
        <Snack name="coca cola" />
        <ShoppingCart />
      </div>
    )
  }
}

export default DragDropContext(HTML5Backend)(Container);
