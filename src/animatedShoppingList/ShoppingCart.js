import React, { Component } from 'react';
import {DropTarget} from 'react-dnd';
import {PropTypes} from 'prop-types';
import Constants from './Constants';


// dropTarget methods
// drop: called when item is dropped
// hover: called when an item is hovered over the component
// canDrop: use it to specify whether a target is able to accept the item

const ShoppingCartSpec = {
  drop(){
    return {name: 'ShoppingCart'};
  }
}

let collect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}

@DropTarget(Constants.SNACK, ShoppingCartSpec, collect)
class ShoppingCart extends Component {
  render(){
    const {canDrop, isOver, connectDropTarget} = this.props;
    const isActive = canDrop && isOver;
    let backgroundColor = '#FFFFFF';
    if (isActive) {
      backgroundColor = '#F7F7BD';
    } else if (canDrop) {
      backgroundColor = '#F7F7F7';
    };

    const style={
      backgroundColor: backgroundColor
    };
    return connectDropTarget(
        <div className='shopping-cart' style={ style }>
          {
            isActive ? "Hummm, Snack!" : "Drag here to order!"
          }
        </div>
    );
  }
}

ShoppingCart.propTypes = {
  connectDropTarget: PropTypes.func.isRequired,
  isOver: PropTypes.bool.isRequired,
  canDrop: PropTypes.bool.isRequired
}

// snack is the drag source
export default ShoppingCart;
